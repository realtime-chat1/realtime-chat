import { createContext, useState, useCallback, useEffect } from 'react'
//  Services
import { baseUrl, postRequest } from '../utils/services'

export const AuthContext = createContext()

export const AuthContextProvider = ({ children }) => {
  const [user, setUser] = useState(null)
  //  register
  const [registerError, setRegisterError] = useState(null)
  const [isRegisterLoading, setIsRegisterLoading] = useState(false)
  const [registerInfo, setRegisterInfo] = useState({
    name: '',
    email: '',
    password: '',
  })
  //  login
  const [loginError, setLoginError] = useState(null)
  const [isLoginLoading, setIsLoginLoading] = useState(false)
  const [loginInfo, setLoginInfo] = useState({
    email: '',
    password: '',
  })

  useEffect(() => {
    const user = JSON.parse(localStorage.getItem('User'))

    user && setUser(user)
  }, [])

  const updateRegisterInfo = useCallback(info => {
    setRegisterInfo(info)
  }, [])

  const updateLoginInfo = useCallback(info => {
    setLoginInfo(info)
  }, [])

  const registerUser = useCallback(
    async e => {
      e.preventDefault()
      setIsRegisterLoading(true)
      setRegisterError(null)

      const response = await postRequest(
        `${baseUrl}/users/register`,
        JSON.stringify(registerInfo)
      )

      setIsRegisterLoading(false)

      if (response.error) {
        return setRegisterError(response)
      }

      localStorage.setItem('User', JSON.stringify(response))
      setUser(response)
    },
    [registerInfo]
  )

  const loginUser = useCallback(
    async e => {
      e.preventDefault()
      setIsLoginLoading(true)
      setLoginError(false)

      const response = await postRequest(
        `${baseUrl}/users/login`,
        JSON.stringify(loginInfo)
      )

      setIsLoginLoading(false)

      if (response.error) {
        return setLoginError(response)
      }

      localStorage.setItem('User', JSON.stringify(response))
      setUser(response)
    },
    [loginInfo]
  )

  const logoutUser = useCallback(() => {
    localStorage.removeItem('User')
    setUser(null)
    setLoginInfo({})
  }, [])

  return (
    <AuthContext.Provider
      value={{
        user,
        registerInfo,
        updateRegisterInfo,
        registerUser,
        registerError,
        isRegisterLoading,
        loginUser,
        loginError,
        isLoginLoading,
        loginInfo,
        updateLoginInfo,
        logoutUser,
      }}
    >
      {children}
    </AuthContext.Provider>
  )
}
