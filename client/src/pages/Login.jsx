

import { useAuthContext } from '../hooks/useAuthContext'

const Login = () => {
  const {
    loginInfo,
    updateLoginInfo,
    loginUser,
    loginError,
    isLoginLoading,
  } = useAuthContext()

  const onInputChange = e => {
    const type = e.target.type
    const name = e.target.name
    const value = type === 'checkbox' ? e.target.checked : e.target.value

    updateLoginInfo(prev => ({ ...prev, [name]: value }))
  }



  return (
    <form className='form' onSubmit={loginUser}>
      <h2>Login</h2>
      <div className='form__control'>
        <label htmlFor='email'>Email</label>
        <input
          type='email'
          id='email'
          name='email'
          placeholder='Your email'
          required
          value={loginInfo.email}
          onChange={onInputChange}
        />
      </div>
      <div className='form__control'>
        <label htmlFor='password'>Password</label>
        <input
          type='password'
          id='password'
          name='password'
          placeholder='Super secret password'
          required
          value={loginInfo.password}
          onChange={onInputChange}
        />
      </div>

      <button className='form__btn'>
        {isLoginLoading ? 'Logging in...' : 'Login'}
      </button>
      {loginError?.error && (
        <p className='error-alert'>{loginError?.message}</p>
      )}
    </form>
  )
}

export default Login
