import { useAuthContext } from '../hooks/useAuthContext'

const Register = () => {
  const {
    registerInfo,
    updateRegisterInfo,
    registerUser,
    registerError,
    isRegisterLoading,
  } = useAuthContext()

  const onInputChange = e => {
    const type = e.target.type
    const name = e.target.name
    const value = type === 'checkbox' ? e.target.checked : e.target.value

    updateRegisterInfo(prev => ({ ...prev, [name]: value }))
  }



  return (
    <form className='form' onSubmit={registerUser}>
      <h2>Register</h2>
      <div className='form__control'>
        <label htmlFor='name'>Name</label>
        <input
          type='text'
          id='name'
          name='name'
          placeholder='Your name'
          required
          value={registerInfo.name}
          onChange={onInputChange}
        />
      </div>
      <div className='form__control'>
        <label htmlFor='email'>Email</label>
        <input
          type='email'
          id='email'
          name='email'
          placeholder='Your email'
          required
          value={registerInfo.email}
          onChange={onInputChange}
        />
      </div>
      <div className='form__control'>
        <label htmlFor='password'>Password</label>
        <input
          type='password'
          id='password'
          name='password'
          placeholder='Super secret password'
          required
          value={registerInfo.password}
          onChange={onInputChange}
        />
      </div>

      <button className='form__btn'>
        {isRegisterLoading ? 'Creating your account' : 'Register'}
      </button>
      {registerError?.error && (
        <p className='error-alert'>{registerError?.message}</p>
      )}
    </form>
  )
}

export default Register
