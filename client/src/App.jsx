import { Routes, Route, Navigate } from 'react-router-dom'
//  Pages
import Chat from './pages/Chat'
import Register from './pages/Register'
import Login from './pages/Login'
import Navbar from './components/Navbar'
//  Hooks
import { useAuthContext } from './hooks/useAuthContext'

function App() {
  const { user } = useAuthContext()
  return (
    <>
      <Navbar />
      <main className='container'>
        <Routes>
          <Route path='/' element={user ? <Chat /> : <Login />} />
          <Route path='/register' element={user ? <Chat /> : <Register />} />
          <Route path='/login' element={user ? <Chat /> : <Login />} />
          <Route path='*' element={<Navigate to='/' />} />
        </Routes>
      </main>
    </>
  )
}

export default App
