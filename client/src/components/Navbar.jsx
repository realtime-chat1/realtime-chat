import { Link } from 'react-router-dom'
//   Hooks
import { useAuthContext } from '../hooks/useAuthContext'

const Navbar = () => {
  const { user, logoutUser } = useAuthContext()

  return (
    <header className='navbar'>
      <div className='navbar-container container'>
        <h1>
          <Link to='/'>Realtime Chat</Link>
        </h1>
        {user && (
          <span className='navbar__status'>Logged in as {user?.name}</span>
        )}
        <nav aria-label='Primary' className='navbar__nav'>
          <ul>
            {user ? (
              <li>
                <Link to='/login' onClick={logoutUser}>
                  Logout
                </Link>
              </li>
            ) : (
              <>
                <li>
                  <Link to='/login'>Login</Link>
                </li>
                <li>
                  <Link to='/register'>Register</Link>
                </li>
              </>
            )}
          </ul>
        </nav>
      </div>
    </header>
  )
}

export default Navbar
