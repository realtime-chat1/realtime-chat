require('dotenv').config()
const express = require('express')
const cors = require('cors')
const mongoose = require('mongoose')
//  Routes imports
const userRoute = require('./Routes/userRoute')
const chatRoute = require('./Routes/chatRoute')
const messageRoute = require('./Routes/messageRoute')

//  .env Variables
const PORT = process.env.PORT || 5000
const uri = process.env.MONGO_URI

const app = express()

//  Middleware
app.use(express.json())
app.use(cors())
app.use('/api/users', userRoute)
app.use('/api/chats', chatRoute)
app.use('/api/messages', messageRoute)

//  Routes
app.get('/', (req, res) => {
    res.send('Welcome to our chat app APIs...')
})

app.listen(PORT, (req, res) => {
  console.log(`Server running on port: ${PORT}`)
})

mongoose.set('strictQuery', false)
mongoose
  .connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log('MongoDB connection established!'))
  .catch(err => console.log('MongoDB connection failed: ', err.message))
